Source: libasync-interrupt-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: gregor herrmann <gregoa@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13),
               libcanary-stability-perl,
               libcommon-sense-perl <!nocheck>,
               perl-xs-dev,
               perl:native
Standards-Version: 4.5.1
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libasync-interrupt-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libasync-interrupt-perl.git
Homepage: https://metacpan.org/release/Async-Interrupt
Rules-Requires-Root: no

Package: libasync-interrupt-perl
Architecture: any
Depends: ${misc:Depends},
         ${perl:Depends},
         ${shlibs:Depends},
         libcommon-sense-perl
Description: module to allow C/XS libraries to interrupt perl
 Async::Interrupt is a Perl module that implements asynchronous interruptions,
 similar in nature to UNIX signals, in a cross-platform manner. Modules might
 want to run code asynchronously (in another thread or from a signal handler)
 and then signal the interpreter on certain events. One common way is to write
 data to a pipe and use an event handling toolkit to watch for I/O events.
 Another way is to send a signal. Those methods are slow, and in the case of a
 pipe, also not asynchronous - it won't interrupt a running Perl interpreter.
 .
 This module implements asynchronous notifications that enable you to signal
 running Perl code from another thread, asynchronously, and sometimes even
 without using a single syscall.
